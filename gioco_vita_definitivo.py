import pygame

def CaricaConfigurazioneIniziale():
    dati = open("Configurazione01.txt", "r")
    for line in dati:
        for c in line:
            if c <> "\n":
                try:
                    pop.append(int(c))
                except:
                    pass

def disegna_griglia():
    for x in range(0, dim_finestra, dim_finestra / dim_u):
        for y in range(0, dim_finestra, dim_finestra / dim_u):
            pygame.draw.line(screen, BLACK, [0, y], [dim_finestra, y], 1)
            pygame.draw.line(screen, BLACK, [x, 0], [x, dim_finestra], 1)

def nuova_generazione():
    pop_temporanea = []
    for n in range(len(pop)):
        pop_temporanea.append(pop[n])
    for x in range(0, len(pop_temporanea)):
        accesi = 0
        if x / dim_u > 0 and x / dim_u < dim_u - 1 and x % dim_u > 0 and x % dim_u < dim_u - 1:
            accesi = accesi + pop_temporanea[x - dim_u - 1]
            accesi = accesi + pop_temporanea[x - dim_u]
            accesi = accesi + pop_temporanea[x - dim_u + 1]
            accesi = accesi + pop_temporanea[x - 1]
            accesi = accesi + pop_temporanea[x + 1]
            accesi = accesi + pop_temporanea[x + dim_u - 1]
            accesi = accesi + pop_temporanea[x + dim_u]
            accesi = accesi + pop_temporanea[x + dim_u + 1]
        if x == 0:
            accesi = accesi + pop_temporanea[x + dim_u]
            accesi = accesi + pop_temporanea[x + 1]
        if x == (dim_u ** 2) - 1:
            accesi = accesi + pop_temporanea[x - dim_u]
            accesi = accesi + pop_temporanea[x - 1]
        if x == dim_u - 1:
            accesi = accesi + pop_temporanea[x + dim_u]
            accesi = accesi + pop_temporanea[x - 1]
        if x == (dim_u ** 2) - dim_u - 1:
            accesi = accesi + pop_temporanea[x - dim_u]
            accesi = accesi + pop_temporanea[x + 1]
        '''
        if not(x / dim_u > 0 and x / dim_u < dim_u - 1) and x <> 0 and x <> (dim_u ** 2) - 1 and x <> dim_u - 1 and x <> (dim_u ** 2) - dim_u - 1:
            accesi = accesi + pop_temporanea[x - 1]
            accesi = accesi + pop_temporanea[x + 1]
            accesi = accesi + pop_temporanea[x + dim_u - 1]
            accesi = accesi + pop_temporanea[x + dim_u]
            accesi = accesi + pop_temporanea[x + dim_u + 1]
        
        if not(x % dim_u > 0 and x % dim_u < dim_u - 1) and x <> 0 and x <> (dim_u ** 2) - 1 and x <> dim_u - 1 and x <> (dim_u ** 2) - dim_u - 1:
            accesi = accesi + pop_temporanea[x - dim_u - 1]
            accesi = accesi + pop_temporanea[x - dim_u]
            accesi = accesi + pop_temporanea[x - dim_u + 1]
            accesi = accesi + pop_temporanea[x - 1]
            accesi = accesi + pop_temporanea[x + 1]
        '''
        if pop_temporanea[x] == 1:
            if accesi < 2 or accesi > 3:
                pop[x] = 0
        if pop_temporanea[x] == 0:
            if accesi == 3:
                pop[x] = 1
def disegna_popolazione():
    dim_cella = dim_finestra / dim_u
    for n in range(0, len(pop)-1):
        if pop[n] == 1:
            pygame.draw.rect(screen, RED, [n % dim_u * dim_cella, n / dim_u * dim_cella, dim_cella, dim_cella])

#definiamo i colori
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED   = (255, 0, 0)
BLUE = (0 ,0, 255)

#avviamo pygame
pygame.init()

#imposto la dimensione della finestra in pixel

dim_finestra = 1200
dim_u = 50
pop = []

# CaricaConfigurazioneIniziale()


for n in range(dim_u**2):
    pop.append(0)

#condizioni iniziali

pop[52] = 1
pop[103] = 1
pop[151] = 1
pop[152] = 1
pop[153] = 1

pop[625] = 1
pop[626] = 1
pop[627] = 1
pop[628] = 1
pop[629] = 1

pop[620] = 1
pop[621] = 1
pop[622] = 1
pop[619] = 1
pop[618] = 1

pop[724] = 1
pop[725] = 1
pop[723] = 1
pop[722] = 1
pop[721] = 1


pop[1507] = 1
pop[1508] = 1
pop[1509] = 1

pop[1535] = 1
pop[1536] = 1
pop[1537] = 1
pop[1538] = 1
pop[1539] = 1
pop[1540] = 1
pop[1541] = 1

screen = pygame.display.set_mode((dim_finestra, dim_finestra))
pygame.display.set_caption("IL GIOCO DELLA VITA  - UN AUTOMA CELLULARE")

done = False

clock = pygame.time.Clock()

#scriviamo il ciclo principale del programma
while not done:
    #gestione della chiusura della finestra
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
    #RIEMPIO LO SPAZIO DI BIANCO
    screen.fill(WHITE)
    #DISEGNAMO IL TUTTE COSE (da qui fino a dove indicato ho modificato il programma)
    disegna_popolazione()
    disegna_griglia()
    nuova_generazione()
    pygame.display.flip()
    clock.tick(20)

# pygame.quit()
